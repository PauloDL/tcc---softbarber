unit USplah;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls;

type
  TSplash = class(TForm)
    Image1: TImage;
    ProgressBar1: TProgressBar;
    Timer1: TTimer;
    Label1: TLabel;
    Panel1: TPanel;
    procedure Timer1Timer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Splash: TSplash;

implementation

{$R *.dfm}
uses UPrincipal, uLogin;

procedure TSplash.FormActivate(Sender: TObject);
begin
          Timer1.Enabled := True;
end;

procedure TSplash.Timer1Timer(Sender: TObject);
begin
          if ProgressBar1.Position = 100
            then Begin
                   Timer1.Enabled := False;
                   Login.ShowModal;
                   Splash.Close;

                 End;

         Label1.Caption := 'Aguarde. Carregando... '
                        + progressbar1.Position.ToString
                        + '%';
         ProgressBar1.Position := ProgressBar1.Position + 1;

end;

end.

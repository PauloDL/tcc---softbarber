unit UPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Mask, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls;

type
  TFPrincipal = class(TForm)
    Image1: TImage;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton4: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPrincipal: TFPrincipal;

implementation

{$R *.dfm}

uses UCad_Cli, UServicos, UPesquisa;

procedure TFPrincipal.SpeedButton1Click(Sender: TObject);
begin
          Application.CreateForm(TCad_Cli, Cad_Cli);
          Cad_Cli.ShowModal;
          Cad_Cli.FDQuery1.Active;
         // Cad_CLi.DBEdit5.DataField('DATA')).EditMask := '99/99/9999;0; ';
end;

procedure TFPrincipal.SpeedButton2Click(Sender: TObject);
begin
          Application.CreateForm(TFPesquisa, FPesquisa);
          FPesquisa.ShowModal;
end;

end.

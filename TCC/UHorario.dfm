object FHorario: TFHorario
  Left = 0
  Top = 0
  Caption = 'Horarios'
  ClientHeight = 394
  ClientWidth = 489
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 298
    Top = 81
    Width = 31
    Height = 13
    Caption = 'Label3'
  end
  object GroupBox1: TGroupBox
    Left = 24
    Top = 56
    Width = 153
    Height = 209
    Caption = 'Hor'#225'rio'
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Trajan Pro 3'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object Label4: TLabel
      Left = 19
      Top = 40
      Width = 36
      Height = 18
      Caption = 'DATA'
      FocusControl = DBEdit1
    end
    object Label1: TLabel
      Left = 19
      Top = 112
      Width = 42
      Height = 18
      Caption = 'HORA'
      FocusControl = DBEdit2
    end
    object DBEdit1: TDBEdit
      Left = 0
      Top = 60
      Width = 134
      Height = 26
      DataField = 'DATA'
      DataSource = DataSource2
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 0
      Top = 136
      Width = 134
      Height = 26
      DataField = 'HORA'
      DataSource = DataSource2
      TabOrder = 1
    end
  end
  object Button1: TButton
    Left = 298
    Top = 346
    Width = 163
    Height = 25
    Caption = 'FINALIZAR CADASTRO'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = Button1Click
  end
  object Timer1: TTimer
    Left = 304
    Top = 16
  end
  object FDQuery1: TFDQuery
    CachedUpdates = True
    MasterSource = DataSource2
    Connection = Modulo.Conexao
    SQL.Strings = (
      'Select * '
      'from horario;')
    Left = 256
    Top = 168
  end
  object DataSource2: TDataSource
    DataSet = Modulo.HorarioTable
    Left = 312
    Top = 168
  end
end

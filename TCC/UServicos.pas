unit UServicos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.StdCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, Vcl.Grids, Vcl.DBGrids,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.DBCtrls, System.Rtti,
  System.Bindings.Outputs, Vcl.Bind.Editors, Data.Bind.EngExt,
  Vcl.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope;

type
  TServicos = class(TForm)
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    Label5: TLabel;
    qAux: TFDQuery;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    qAuxID_SERV: TIntegerField;
    qAuxTP_SERVICO: TIntegerField;
    qAuxSERVICOS: TStringField;
    qAuxVALOR: TCurrencyField;
    BitBtn1: TBitBtn;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    FDQuery1: TFDQuery;
    FDQuery1ID_CLI: TIntegerField;
    FDQuery1ID_SERV: TIntegerField;
    FDQuery1ID_HOR: TIntegerField;
    DBGrid2: TDBGrid;
    DataSource2: TDataSource;
    BindSourceDB2: TBindSourceDB;
    LinkFillControlToField1: TLinkFillControlToField;
    DBLookupComboBox1: TDBLookupComboBox;
    Button1: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBLookupComboBox2: TDBLookupComboBox;
    Label4: TLabel;
    Button2: TButton;
    Button3: TButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure DBLookupComboBox2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    CLIID  : Integer;
    CLINOM : String;
    CLIHOR : Integer;
    CLISER : Integer;
  end;

var
  Servicos: TServicos;

implementation

{$R *.dfm}

uses UModulo, UHorario;

procedure TServicos.BitBtn1Click(Sender: TObject);
begin
    Application.CreateForm(TFHorario, FHorario);
    FHorario.ShowModal;
end;

procedure TServicos.Button1Click(Sender: TObject);
begin
          Modulo.ClienteTable.RecNo;
          CLIID  := Modulo.ClienteTableID_CLI.AsInteger;
          CLINOM := Modulo.ClienteTableNOME.AsString;
          Label1.Caption := CLINOM;
end;

procedure TServicos.Button2Click(Sender: TObject);
begin
          FDQuery1.Insert;

          FDQuery1ID_CLI.AsInteger  := CLIID;
          FDQuery1ID_SERV.AsInteger := CLISER;
          FDQuery1ID_HOR.AsInteger  := CLIHOR;

          FDQuery1.Post;
          FDQuery1.CommitUpdates;
end;

procedure TServicos.Button3Click(Sender: TObject);
begin
          FDQuery1.Cancel;
end;

procedure TServicos.DBGrid1DblClick(Sender: TObject);
begin
          Modulo.ServicoTable.RecNo;

          CLISER := Modulo.ServicoTableID_SERV.AsInteger;

end;

procedure TServicos.DBLookupComboBox2Click(Sender: TObject);
begin
          Modulo.HorarioTable.RecNo;
          CLIHOR := Modulo.HorarioTableID_HOR.AsInteger;
end;

procedure TServicos.FormCreate(Sender: TObject);
begin
          FDQuery1.Active := True;
end;

procedure TServicos.SpeedButton1Click(Sender: TObject);
Var       Texto : String;
          NReg  : Integer;
begin
          Texto := 'SELECT * '
                +  'FROM  SERVICO '
                +  'WHERE SERVICO.tp_servico = :PSERVICO';

          qAux.Close;
          qAux.SQL.Clear;
          qAux.SQL.Add(Texto);
          qAux.ParamByName('PSERVICO').AsInteger := 3;
          qAux.Open;
end;

procedure TServicos.SpeedButton2Click(Sender: TObject);
Var       Texto : String;
          NReg  : Integer;
begin
          Texto := 'SELECT * '
                +  'FROM  SERVICO '
                +  'WHERE SERVICO.tp_servico = :PSERVICO';

          qAux.Close;
          qAux.SQL.Clear;
          qAux.SQL.Add(Texto);
          qAux.ParamByName('PSERVICO').AsInteger := 2;
          qAux.Open;

end;

procedure TServicos.SpeedButton3Click(Sender: TObject);
Var       Texto : String;
          NReg  : Integer;
begin
          Texto := 'SELECT * '
                +  'FROM  SERVICO '
                +  'WHERE SERVICO.tp_servico = :PSERVICO';

          qAux.Close;
          qAux.SQL.Clear;
          qAux.SQL.Add(Texto);
          qAux.ParamByName('PSERVICO').AsInteger := 1;
          qAux.Open;

end;

end.

program SoftBarber;

uses
  Vcl.Forms,
  uLogin in 'uLogin.pas' {Login},
  UPrincipal in 'UPrincipal.pas' {FPrincipal},
  UCad_Cli in 'UCad_Cli.pas' {Cad_Cli},
  UServicos in 'UServicos.pas' {Servicos},
  UHorario in 'UHorario.pas' {FHorario},
  USplah in 'USplah.pas' {Splash},
  UPesquisa in 'UPesquisa.pas' {FPesquisa},
  UModulo in 'UModulo.pas' {Modulo: TDataModule},
  Unit2 in 'Unit2.pas' {Form2},
  UModuloR in 'UModuloR.pas' {DataModule3: TDataModule};

{$R *.res}

Var        I : Integer;
begin

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TModulo, Modulo);
  Application.CreateForm(TDataModule3, DataModule3);
  Modulo.Conexao.Connected := True;

    Modulo.AdmTable.Active := True;
    Modulo.HorarioTable.Active  := True;
    Modulo.BairroTable.Active   := True;
    Modulo.ClienteTable.Active  := True;
    Modulo.ServicoTable.Active  := True;
    Modulo.CidadeTable.Active   := True;
    Modulo.EstadoTable.Active   := True;
    Modulo.PaisTable.Active     := True;
    Modulo.EnderecoTable.Active := True;


  Application.CreateForm(TSplash, Splash);
  Application.CreateForm(TLogin, Login);
  Application.CreateForm(TFPrincipal, FPrincipal);
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.

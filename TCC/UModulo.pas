unit UModulo;

interface

uses
  System.SysUtils, System.Classes, Data.FMTBcd, Datasnap.Provider, Data.DB,
  Data.SqlExpr, Data.DBXMySQL, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet;

type
  TModulo = class(TDataModule)
    Conexao: TFDConnection;
    AdmTable: TFDTable;
    HorarioTable: TFDTable;
    BairroTable: TFDTable;
    ClienteTable: TFDTable;
    ServicoTable: TFDTable;
    CidadeTable: TFDTable;
    EstadoTable: TFDTable;
    PaisTable: TFDTable;
    EnderecoTable: TFDTable;

    dsADM: TDataSource;
    dsBairro: TDataSource;
    dsCliente: TDataSource;
    dsServicos: TDataSource;
    dsHorarios: TDataSource;
    dsCidade: TDataSource;
    dsEstado: TDataSource;
    dsPaises: TDataSource;
    dsEndereco: TDataSource;
    AdmTableID_ADM: TIntegerField;
    AdmTableUSUARIO: TStringField;
    AdmTableSENHA: TStringField;
    HorarioTableID_HOR: TIntegerField;
    HorarioTableDATA: TDateField;
    HorarioTableHORA: TTimeField;
    BairroTableID_BAI: TIntegerField;
    BairroTableNOME: TStringField;
    BairroTableID_CID: TIntegerField;
    CidadeTableID_CID: TIntegerField;
    CidadeTableNOME: TStringField;
    CidadeTableSIGLA: TStringField;
    CidadeTableID_EST: TIntegerField;
    EstadoTableID_EST: TIntegerField;
    EstadoTableNOME: TStringField;
    EstadoTableSIGLA: TStringField;
    EstadoTableID_PAIS: TIntegerField;
    PaisTableID_PAIS: TIntegerField;
    PaisTableNOME: TStringField;
    PaisTableSIGLA: TStringField;
    EnderecoTableID_END: TIntegerField;
    EnderecoTableRUA: TStringField;
    EnderecoTableCOMPLEMENTO: TStringField;
    EnderecoTableCEP: TStringField;
    EnderecoTableID_BAI: TIntegerField;
    ServicoTableID_SERV: TIntegerField;
    ServicoTableSERVICOS: TStringField;
    ServicoTableVALOR: TCurrencyField;
    ClienteTableID_CLI: TIntegerField;
    ClienteTableNOME: TStringField;
    ClienteTableEMAIL: TStringField;
    ClienteTableTELEFONE: TStringField;
    ClienteTableDATA_CA: TStringField;
    ClienteTableID_END: TIntegerField;
    ServCliTable: TFDTable;
    ServCliTableID_CLI: TIntegerField;
    ServCliTableID_SERV: TIntegerField;
    ServCliTableID_HOR: TIntegerField;
    dsServCli: TDataSource;
    procedure HorarioTableAfterOpen(DataSet: TDataSet);
    procedure ClienteTableAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Modulo: TModulo;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TModulo.ClienteTableAfterOpen(DataSet: TDataSet);
begin
TStringField(DataSet.FieldByName('TELEFONE')).EditMask := '(99)09999-9999;0; ';
end;

procedure TModulo.HorarioTableAfterOpen(DataSet: TDataSet);
begin
	TStringField(DataSet.FieldByName('DATA')).EditMask := '99/99/9999;0; ';


end;

end.

unit UCad_Cli;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.StdCtrls, Vcl.Mask,
  Vcl.DBCtrls, Data.DB, UModulo, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, System.Rtti,
  System.Bindings.Outputs, Data.Bind.EngExt, Vcl.Bind.DBEngExt,
  Data.Bind.Components, Data.Bind.DBScope;

type
  TCad_Cli = class(TForm)
    Label5: TLabel;
    Label7: TLabel;
    SpeedButton1: TSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label9: TLabel;
    DBEdit3: TDBEdit;
    DBEdit1: TDBEdit;
    DBNavigator1: TDBNavigator;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    btnInserir: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    qAux: TFDQuery;
    DBLookupComboBox1: TDBLookupComboBox;
    DBGrid1: TDBGrid;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    BindSourceDB2: TBindSourceDB;
    BindSourceDB3: TBindSourceDB;
    DataSource1: TDataSource;
    FDQuery1: TFDQuery;
    FDQuery1ID_END: TIntegerField;
    FDQuery1RUA: TStringField;
    FDQuery1COMPLEMENTO: TStringField;
    FDQuery1CEP: TStringField;
    FDQuery1ID_BAI: TIntegerField;
    FDQuery1NBAIRRO: TStringField;
    FDQuery1NCIDADE: TStringField;
    FDQuery1CSIGLA: TStringField;
    FDQuery1NESTADO: TStringField;
    FDQuery1ESIGLA: TStringField;
    FDQuery1NPAIS: TStringField;
    FDQuery1PSIGLA: TStringField;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    procedure btnInserirClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Cad_Cli: TCad_Cli;

implementation

{$R *.dfm}

uses UServicos, UPrincipal;

procedure TCad_Cli.btnInserirClick(Sender: TObject);
Var       Texto : String;
          NReg  : Integer;
begin
          Texto := 'SELECT MAX(ID_CLI) AS ULTIMO FROM CLIENTE';
          qAux.Close;
          qAux.SQL.Clear;
          qAux.SQL.Add(Texto);
          qAux.Open;

          if qAux.FieldByName('ULTIMO').AsInteger = null
             then Nreg := 1
             else NReg := qAux.FieldByName('ULTIMO').AsInteger + 1;

          Modulo.ClienteTable.Insert;
          Modulo.ClienteTableID_CLI.AsInteger := NReg;
          dbEdit3.SetFocus;

end;

procedure TCad_Cli.Button2Click(Sender: TObject);
begin
          Modulo.ClienteTable.Edit;
          dbEdit3.SetFocus;
end;

procedure TCad_Cli.Button3Click(Sender: TObject);
begin
          If MessageDlg ('Deletar Registro',MTWarning,[MBYes, MBNO],0)  = MRYes
             Then Begin
                    Modulo.ClienteTable.Delete;
                    //Modulo.ClienteTable.ApplyUpdates(-1);
                    Modulo.ClienteTable.CommitUpdates;
                    ShowMessage ('Exclu�do com Sucesso');
                  End
             Else ShowMessage ('N�o Exclu�do')
end;

procedure TCad_Cli.Button4Click(Sender: TObject);
begin
          Modulo.ClienteTable.Cancel;
end;

procedure TCad_Cli.Button5Click(Sender: TObject);
begin
          Modulo.ClienteTable.Post;
          Modulo.ClienteTable.ApplyUpdates(-1);
          //Modulo.ClienteTable.CommitUpdates;
end;

procedure TCad_Cli.FormClose(Sender: TObject; var Action: TCloseAction);
begin
          FPrincipal.Show;
end;

procedure TCad_Cli.SpeedButton1Click(Sender: TObject);
begin
          Application.CreateForm(TServicos, Servicos);
          Servicos.ShowModal;
end;

end.
